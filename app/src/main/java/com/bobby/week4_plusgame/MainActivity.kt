package com.bobby.week4_plusgame

import android.annotation.SuppressLint
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.view.isInvisible
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.concurrent.schedule
import kotlin.concurrent.timerTask
import kotlin.math.log
import kotlin.random.Random.Default.nextInt

public var correct: Int = 0
public var incorrect: Int = 0
public var isSelectAnswer = false
class MainActivity : AppCompatActivity() {
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
            play()
    }

    fun play() {
        val txtNum1 = findViewById<TextView>(R.id.txtNum1)
        val txtNum2 = findViewById<TextView>(R.id.txtNum2)
        val txtResult = findViewById<TextView>(R.id.txtResult)
        val txtStatic = findViewById<TextView>(R.id.txtStatic)
        val txtAnswer = findViewById<TextView>(R.id.txtAnswer)

        txtAnswer.text = ""

        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)
        val btnPlay = findViewById<Button>(R.id.btnPlay)
        btnPlay.visibility = View.GONE

        btn1.setBackgroundColor(Color.WHITE)
        btn2.setBackgroundColor(Color.WHITE)
        btn3.setBackgroundColor(Color.WHITE)

        isSelectAnswer = false

        txtNum1.text = (0..10).random().toString()
        txtNum2.text = (0..10).random().toString()

        val randomBtn = (1..3).random().toString()
        val num1 = (txtNum1).text.toString().toInt()
        val num2 = (txtNum2).text.toString().toInt()
        if (randomBtn == "1") {
            btn1.text = (num1 + num2).toString()

            var randomNum1 = (0..(num1 + num2)-1).random().toString()
            var randomNum2 = ((num1 + num2)+1..20).random().toString()
            btn2.text = (randomNum1.toInt()).toString()
            btn3.text = (randomNum2.toInt()).toString()
        } else if (randomBtn == "2") {
            btn2.text = (num1 + num2).toString()
            var randomNum1 = (0..(num1 + num2)-1).random().toString()
            var randomNum2 = ((num1 + num2)+1..20).random().toString()
            btn1.text = (randomNum1.toInt()).toString()
            btn3.text = (randomNum2.toInt()).toString()
        } else {
            btn3.text = (num1 + num2).toString()
            var randomNum1 = (0..(num1 + num2)-1).random().toString()
            var randomNum2 = ((num1 + num2)+1..20).random().toString()
            btn1.text = (randomNum1.toInt()).toString()
            btn2.text = (randomNum2.toInt()).toString()
        }


        btn1.setOnClickListener {
            if (randomBtn == "1") {
                showWin(btn1)
            } else {
                showLose(btn1)
            }
        }
        btn2.setOnClickListener {
            if (randomBtn == "2") {
                showWin(btn2)
            } else {
                showLose(btn2)
            }
        }
        btn3.setOnClickListener {
            if (randomBtn == "3") {
                showWin(btn3)
            } else {
                showLose(btn3)
            }
        }
        btnPlay.setOnClickListener {
            play()
        }
    }

    private fun showLose(
        btn: Button
    ) {
        if(!isSelectAnswer){
            btn.setBackgroundColor(Color.RED)
            txtAnswer.text = "You're wrong"
            incorrect++
            txtStatic.text = "correct:${correct} incorrect:${incorrect}"
            btnPlay.visibility = View.VISIBLE
            isSelectAnswer =  true


        }

    }
    private fun showWin(
        btn: Button
    ) {
        if(!isSelectAnswer){
            btn.setBackgroundColor(Color.GREEN)
            txtAnswer.text = "You're right"
            correct++
            txtStatic.text = "correct:${correct} incorrect:${incorrect}"
            btnPlay.visibility = View.VISIBLE
            isSelectAnswer =  true
        }

    }
}